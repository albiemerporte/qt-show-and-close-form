
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication

class Main:
    def __init__(self, inform1):
        self.mainui = inform1
        self.mainui.show()
        self.form2 = loadUi('form2.ui')
        self.mainui.pbShowForm2.clicked.connect(self.showform2)
        self.form2.pbShowForm1.clicked.connect(self.showform1)

    def showform2(self):
        self.mainui.close()
        self.form2.show()

    def showform1(self):
        self.form2.close()
        self.mainui.show()

if __name__ == '__main__':
    app = QApplication([])
    ui = loadUi('form1.ui')
    main = Main(ui)
    app.exec()